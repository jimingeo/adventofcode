'''Day 7: File system parser'''

import sys
from dataclasses import dataclass
from typing import Optional, List

COMMAND_OUTPUT = 'output'
COMMAND_NAME = 'command'

TOTAL_AVAILABLE_SIZE = 70000000
REQUIRED_FREE_SPACE = 30000000

@dataclass
class File:
    file_name: str
    file_size: int

class Directory:
    def __init__(self, name: str='/', parent=None) -> None:
        self.name: str = name
        self.files: List[Optional[File]] = []
        self.directories: List[Optional[Directory]] = []
        self.parent = parent
        self.path_name = name if not parent else f'{parent.path_name}/{name}'

    def add_directory(self, name: str) -> None:
        directory = Directory(name=name, parent=self)
        self.directories.append(directory)

    def add_file(self, file_: File) -> None:
        self.files.append(file_)

class FileSystem:
    def __init__(self) -> None:
        self._root_directory: Directory = Directory()
        self._current_directory: Directory = self._root_directory

    def change_directory(self, directory_name: str) -> None:
        if directory_name == '/':
            self._current_directory = self._root_directory
        elif directory_name == '..':
            self._current_directory = self._current_directory.parent
        else:
            for _directory in self._current_directory.directories:
                if _directory.name == directory_name:
                    self._current_directory = _directory

    def add_directory(self, name: str) -> None:
        self._current_directory.add_directory(name)

    def add_file(self, file_: File) -> None:
        self._current_directory.add_file(file_)

    def total_size(self, directory) -> int:
        return (
            sum([self.total_size(child_dir) for child_dir in directory.directories]) +
            sum([file_.file_size for file_ in directory.files]))

    def get_size_of_all_directories(self) -> dict[str, int]:
        return {
            '/': self.total_size(self._root_directory),
            **self.get_size_of_subdirectories(self._root_directory)}

    def get_size_of_subdirectories(self, directory) -> dict[str, int]:
        directory_sizes = {}
        for dir_ in directory.directories:
            directory_sizes[dir_.path_name] = self.total_size(dir_)
            directory_sizes = {**directory_sizes, **self.get_size_of_subdirectories(dir_)}
        return directory_sizes

def read_input():
    for line in sys.stdin:
        yield line.rstrip('\n')

def parse_input(data):
    # Expects an iterator/generator input
    command_output = {}
    for line in data:
        if line.startswith('$'):
            # This is a command. The lines following this will be response of this command.
            if command_output:
                # Return previously gathered command output...
                yield command_output
                # ...then reset the data for new output
            command_output = {
                COMMAND_NAME: line.split()[1:],
                COMMAND_OUTPUT: []
            }
            continue
        command_output[COMMAND_OUTPUT].append(line.split())
    yield command_output    # Return the final command output

def process_command_outputs(command_output: dict[str, list[str]], file_system: FileSystem):
    if command_output[COMMAND_NAME][0] == 'cd':
        file_system.change_directory(command_output[COMMAND_NAME][1])
    elif command_output[COMMAND_NAME][0] == 'ls':
        for list_param, list_item in command_output[COMMAND_OUTPUT]:
            if list_param == 'dir':
                file_system.add_directory(list_item)
            else:
                file_system.add_file(
                    File(file_name=list_item,
                         file_size=int(list_param)))


if __name__ == "__main__":
    device_file_system = FileSystem()
    for l in parse_input(read_input()):
        process_command_outputs(l, device_file_system)
    dir_sizes = device_file_system.get_size_of_all_directories()
    dirs_with_max_100000_size = [
        value for key, value in
        dir_sizes.items()
        if value <= 100000]
    print(f'Day 7, part 1: {sum(dirs_with_max_100000_size)}')
    #part 2
    available_free_space = TOTAL_AVAILABLE_SIZE - dir_sizes['/']
    required_space = REQUIRED_FREE_SPACE - available_free_space
    deletable_candidates = {dir_: size for dir_, size in dir_sizes.items() if size >= required_space}
    print(f'Day 7, part 2: {sorted(deletable_candidates.values())[0]}')
