#!/usr/bin/env python
'''Day 5: Crate stack processor
'''

import copy
import sys

def read_input():
    for line in sys.stdin:
        yield line.rstrip('\n')

def parse_input(data):
    # Expects an iterator/generator input
    stack_data = []
    for line in data:
        if line:
            stack_data.append(line)
        else:
            break
    move_data = []
    for line in data:
        move_data.append(line.split())
    return parse_stack(stack_data), parse_moves(move_data)

def parse_stack(data):
    stacks = []
    for line in data[:-1]:  # Skip the index of the stacks
        stacks.append([line[i+1:i+2] for i in range(0, len(line), 4)])
    return [[item for item in stack[::-1] if item != ' '] for stack in zip(*stacks)]

def parse_moves(data):
    # Subtract 1 from the source and destination stacks to match list index
    return [(int(move[1]), int(move[3]) - 1, int(move[5]) - 1) for move in data]

def take_action_cratemover9000(move, stacks):
    count, from_stack, to_stack = move
    for _ in range(0, count):
        stacks[to_stack].append(stacks[from_stack].pop())

def take_action_cratemover9001(move, stacks):
    count, from_stack, to_stack = move
    crane = []
    for _ in range(0, count):
        crane.append(stacks[from_stack].pop())
    crane.reverse()
    stacks[to_stack].extend(crane)

if __name__ == '__main__':
    crate_stacks, crane_actions = parse_input(read_input())
    new_crate_stacks = copy.deepcopy(crate_stacks)
    for crane_action in crane_actions:
        take_action_cratemover9000(crane_action, crate_stacks)
    print(f"Day 5, part 1 solution: {''.join([stack[-1] for stack in crate_stacks])}")
    for crane_action in crane_actions:
        take_action_cratemover9001(crane_action, new_crate_stacks)
    print(f"Day 5, part 2 solution: {''.join([stack[-1] for stack in new_crate_stacks])}")
