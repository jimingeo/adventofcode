// Day 2 - Rock-Paper-Scissors cheatsheet decryptor
use std::io;

#[derive(Clone)]
enum Move {
    Rock,
    Paper,
    Scissor
}

struct Game {
    opponent_move: Move,
    my_move: Move
}

fn main() {
    let buffer = io::stdin().lines();
    let mut inp_data = Vec::new();
    for line in buffer {
        inp_data.push(line.unwrap());
    }
    let games = parse_input(inp_data.clone()); //Ugly hack to get around ownership. Should not be cloning here!!
    let result = games.iter().fold(0, |acc, game| {
        acc + move_score(&game.my_move) + game_outcome_score(game)
    });
    println!("Day 2, part 1: Final score: {}", result);
    let new_games = parse_input_for_corrected_code(inp_data);
    let result = new_games.iter().fold(0, |acc, game| {
        acc + move_score(&game.my_move) + game_outcome_score(game)
    });
    println!("Day 2, part 2: Final score: {}", result);
}

fn parse_input(inp_data: Vec<String>) -> Vec<Game> {
    // Convert input list of moves into a Game vector consisting of moves of both players
    let mut games = Vec::new();
    for inp_line in inp_data {
        let inp_line_chars: Vec<char> = inp_line.chars().collect();
        let opponent_move: Move;
        let my_move: Move;
        match inp_line_chars[0] {
            'A' => opponent_move = Move::Rock,
            'B' => opponent_move = Move::Paper,
            'C' => opponent_move = Move::Scissor,
            // Default should never happen, just added to make compiler happy
            _   => opponent_move = Move::Rock
        }
        match inp_line_chars[2] {
            'X' => my_move = Move::Rock,
            'Y' => my_move = Move::Paper,
            'Z' => my_move = Move::Scissor,
            // Default should never happen, just added to make compiler happy
            _   => my_move = Move::Rock
        }
        games.push(Game{opponent_move, my_move});
    }
    games
}

fn parse_input_for_corrected_code(inp_data: Vec<String>) -> Vec<Game> {
    // Convert the input list of moves into a Game vector consisting of opponent move and player
    // move required for getting desired outcome
    let mut games = Vec::new();
    for inp_line in inp_data {
        let inp_line_chars: Vec<char> = inp_line.chars().collect();
        let opponent_move: Move;
        match inp_line_chars[0] {
            'A' => opponent_move = Move::Rock,
            'B' => opponent_move = Move::Paper,
            'C' => opponent_move = Move::Scissor,
            // Default should never happen, just added to make compiler happy
            _   => opponent_move = Move::Rock
        }
        let my_move = get_move_for_desired_outcome(opponent_move.clone(), inp_line_chars[2]);
        games.push(Game{opponent_move, my_move});
    }
    games
}

fn get_move_for_desired_outcome(opponent_move: Move, desired_outcome: char) -> Move {
    match desired_outcome {
        'X' => loosing_move(opponent_move),
        'Y' => draw_move(opponent_move),
        'Z' => winning_move(opponent_move),
        // Default should never happen, just added to make compiler happy
        _   => opponent_move
    }
}

fn winning_move(opponent_move: Move) -> Move {
    match opponent_move {
        Move::Rock => Move::Paper,
        Move::Paper => Move::Scissor,
        Move::Scissor => Move::Rock
    }
}

fn loosing_move(opponent_move: Move) -> Move {
    match opponent_move {
        Move::Rock => Move::Scissor,
        Move::Paper => Move::Rock,
        Move::Scissor => Move::Paper
    }
}

fn draw_move(opponent_move: Move) -> Move {
    opponent_move
}

fn move_score(player_move: &Move) -> u32 {
    // Calculate points for each move; 1 pt for Rock, 2 for paper and 3 for scissor
    match player_move {
        Move::Rock    => 1,
        Move::Paper   => 2,
        Move::Scissor => 3
    }
}

fn game_outcome_score(game: &Game) -> u32 {
    // Calculate game outcome points; 6 points for winning, 3 for draw and 0 for loosing.
    match game {
        Game {
            opponent_move: Move::Rock,
            my_move: Move::Paper}       => 6, //Win
        Game {
            opponent_move: Move::Rock,
            my_move: Move::Scissor}     => 0, //Loss
        Game {
            opponent_move: Move::Paper,
            my_move: Move::Rock}        => 0, //Loss
        Game {
            opponent_move: Move::Paper,
            my_move: Move::Scissor}     => 6, //Win
        Game {
            opponent_move: Move::Scissor,
            my_move: Move::Rock}        => 6, //Win
        Game {
            opponent_move: Move::Scissor,
            my_move: Move::Paper}       => 0, //Loss
        // All other combinations result in draw
        _                               => 3,
    }
}
