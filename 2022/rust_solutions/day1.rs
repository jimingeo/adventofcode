// Day 1: Calorie calculator for snacks with elves
use std::io;


fn main() {
    let buffer = io::stdin().lines();
    let mut inp_data = Vec::new();
    for line in buffer {
        inp_data.push(line.unwrap());
    }
    let elves_calories = parse_input(inp_data);
    let mut total_cal_elves = total_calories_per_elf(elves_calories);
    total_cal_elves.sort();
    println!("Part 1: Max calorie carried by elf: {}", total_cal_elves.last().unwrap());
    let sum_top_three_cals: u32 = total_cal_elves.iter().rev().take(3).sum();
    println!("Part 2: Sum of max three calories: {}", sum_top_three_cals);
}

fn parse_input(input_lines: Vec<String>) -> Vec<Vec<u32>> {
    // Translate the input lines of string (separated by blank lines per elf) into a vector of
    // vectors. Each inner vector will contain a list of calories of each snack carried by an elf,
    // and the outer vector contains the list for each elf.
    let mut elves_calories = Vec::new();
    let mut calorie_per_elf = Vec::new();
    for line in input_lines {
        if line != "" {
            let calorie: u32 = line.parse().unwrap();
            calorie_per_elf.push(calorie);
        } else {
            elves_calories.push(calorie_per_elf.clone());
            calorie_per_elf.clear();
        }
    }
    // Push the last element in. Special handling because there is no space after the last record.
    elves_calories.push(calorie_per_elf.clone());
    elves_calories
}

fn total_calories_per_elf(elves_calories: Vec<Vec<u32>>) -> Vec<u32> {
    // Calculate the total calories held by each elf. Returns a vector of total calories per elf.
    let mut total_cal_per_elf = Vec::new();
    for elv_cals in elves_calories {
        total_cal_per_elf.push(elv_cals.iter().sum());
    }
    total_cal_per_elf
}
