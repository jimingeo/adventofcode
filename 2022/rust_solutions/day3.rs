use std::convert::TryInto;
use std::io;

const PRIORITIES: [char; 52] = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

fn main() {
    let buffer = io::stdin().lines();
    let mut inp_data = Vec::new();
    for line in buffer {
        inp_data.push(line.unwrap());
    }
    println!("Day 3, part 1 solution: {}", inp_data.iter().map(|line| common_item_priority(&line)).sum::<u32>());
}

fn get_priority_for_item_type(item_type: char) -> u32 {
    let priority: u32 = PRIORITIES.iter()
              .position(|&x| x == item_type)
              .unwrap()     // gives usize
              .try_into()   // convert to Option(<u32>)
              .unwrap();    // extract the u32 value
    priority + 1
}

fn parse_rucksack(rucksack: &String) -> (&str, &str) {
    rucksack.split_at(rucksack.len() / 2)
}

fn get_common_elements(rucksack1: &str, rucksack2: &str) -> char {
    // Assuming there can be only one common element
    for c in rucksack1.chars() {
        if rucksack2.contains(c) {
            return c
        }
    }
    ' ' // Ugly hack!! Dummy result for satisfying compiler
}

fn common_item_priority(inp_line: &String) -> u32 {
    let (comp1, comp2) = parse_rucksack(inp_line);
    get_priority_for_item_type(get_common_elements(comp1, comp2))
}
