# Minimal nix configuration
let
    myNixPkgs = import <nixpkgs> {};
in
    myNixPkgs.mkShell {
        buildInputs = with myNixPkgs; [
            entr cargo rustc
        ];
    }
