-- Day 8: Tree house visibility
import Data.List(transpose)

data Coordinates = Coordinates Int Int deriving (Show, Eq)

data Tree = Tree {
    height::Char,
    coordinates::Coordinates
} deriving (Show, Eq)

main = do
    content <- getContents
    let treeMap = parseTrees $ lines content
    putStrLn "Day 8, part 1 solution:"
    print $ length $ uniqueItems $ concat $ getAllVisibleTrees treeMap
    putStrLn "Day 8, part 2 solution:"
    print $ maximum $ concat $ getVisibilityScores treeMap

parseTrees :: [String] -> [[Tree]]
parseTrees rows = zipWith buildTree rows [0..] where
    buildTree trees rowId = zipWith Tree trees (map (Coordinates rowId) [0..(length trees)])

visibleTrees :: Char -> [Tree] -> [Tree]
visibleTrees _ [] = []
visibleTrees tallest (x@(Tree t1 coord1):xs)
    | t1 > tallest = x:visibleTrees t1 xs
    | otherwise = visibleTrees tallest xs

getAllVisibleTrees :: [[Tree]] -> [[Tree]]
getAllVisibleTrees trees =
    concat $ take 4 $ map (map (visibleTrees '-')) (iterate rotateViewLeft trees)

uniqueItems [] = []
uniqueItems [x] = [x]
uniqueItems (x:xs)
    | x `elem` xs = uniqueItems xs    -- Drop x if it is already present
    | otherwise = x:uniqueItems xs

rotateViewLeft = reverse . transpose

rotateViewRight = transpose . reverse

countVisibleTreesOnRight :: [Tree] -> Int
countVisibleTreesOnRight [tree] = 0
countVisibleTreesOnRight (tree1@(Tree t1 _):(Tree t2 _):trees)
    | t2 < t1 = 1 + countVisibleTreesOnRight (tree1:trees)
    | otherwise = 1

getVisibleTreesOnRight :: [Tree] -> [Int]
getVisibleTreesOnRight [] = []
getVisibleTreesOnRight treesRow =
    countVisibleTreesOnRight treesRow: getVisibleTreesOnRight (tail treesRow)

getVisibilityScores :: [[Tree]] -> [[Int]]
getVisibilityScores treesMap =
    productOfScores [visiblityFromRight,
        visibilityFromTop,
        visibilityFromLeft,
        visibilityFromBottom] where
    visibilityFromLeft = map getVisibleTreesOnRight treesMap
    visibilityFromTop = rotateViewRight $ map getVisibleTreesOnRight (rotateViewLeft treesMap)
    visibilityFromBottom = (rotateViewRight . rotateViewRight) $ map getVisibleTreesOnRight ((rotateViewLeft . rotateViewLeft) treesMap)
    visiblityFromRight = rotateViewLeft $ map getVisibleTreesOnRight (rotateViewRight treesMap)

productOfScores :: [[[Int]]] -> [[Int]]
productOfScores ([]:_) = [[]]   -- Adds an unnecessary empty list at the end, but we're going to concat it anyway, so no worries
productOfScores scores = productOfRows (map head scores): productOfScores (map tail scores)

productOfRows :: [[Int]] -> [Int]
productOfRows ([]:_) = []   -- if first list is empty, all should also be
productOfRows rows = (product . map head) rows:productOfRows (map tail rows)
