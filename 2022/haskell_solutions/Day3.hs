import Data.List(nub)

main = do
    content <- getContents
    let ruckSacks = map parseRuckSackContents (lines content)
    let commonItems = map (nub . commonItemsInBag) ruckSacks
    putStrLn "Day 3, part 1 solution:"
    -- Sum of the priorities of each common item per bag
    print $ sum $ map (sum . map getPriorityOfItem) commonItems

    -- Part 2
    let ruckSacksForElfGroups = groupIntoThree (lines content)
    -- let commonItemsForElves = map (nub . commonItemsForElves) ruckSacksForElfGroups
    let commonItemsWithinElfGroup = map (nub . commonItemsForElves) ruckSacksForElfGroups
    putStrLn "Day 3, part 2 solution:"
    print $ sum $ map (sum . map getPriorityOfItem) commonItemsWithinElfGroup

parseRuckSackContents :: String -> (String, String)
parseRuckSackContents inputLine =
    splitAt len inputLine where
        len = div (length inputLine) 2

getPriorityOfItem :: Char -> Int
getPriorityOfItem element =
    let priorityList = ['a'..'z'] ++ ['A'..'Z']
    in length (takeWhile (/= element) priorityList) + 1

commonItemsInBag :: (String, String) -> [Char]
commonItemsInBag (leftCompartment, rightCompartment) =
    filter (`elem` rightCompartment) leftCompartment

groupIntoThree :: [String] -> [(String, String, String)]
-- No error handling because we have no defined behaviour in the problem if input list is not
-- divisible by 3
groupIntoThree [] = []
groupIntoThree inputLines =
    (first, second, third):groupIntoThree (drop 3 inputLines) where
        [first, second, third] = take 3 inputLines

commonItemsForElves :: (String, String, String) -> [Char]
commonItemsForElves (first, second, third) = commonItemsInBag (first, commonItemsInBag (second, third))
