--Day 9: Rope physics
import Data.List(nub)

data Move =
    MoveRight Int |
    MoveLeft Int |
    MoveUp Int |
    MoveDown Int deriving Show

data Point = Point Int Int deriving (Show, Eq)

data Rope = Rope {
    ropeHead::Point,
    ropeTail::Point
} deriving Show


main = do
    content <- getContents
    let steps = concatMap (getSteps . parseMove) (lines content)
    let rope = Rope (Point 0 0) (Point 0 0)
    putStrLn "Day 9, part 1 solution"
    print $ (length . nub . map (ropeTail . last)) (applyMoves [rope] steps)
    let ropeWithKnots = replicate 10 rope
    putStrLn "Day 9, part 2 solution"
    print $ (length . nub . map (ropeHead . last)) (applyMoves ropeWithKnots steps)

parseMove :: String -> Move
parseMove (x:' ':steps) = parseFunc x (read steps::Int) where
    parseFunc 'R' = MoveRight
    parseFunc 'L' = MoveLeft
    parseFunc 'U' = MoveUp
    parseFunc 'D' = MoveDown

applyMoves :: [Rope] -> [Point] -> [[Rope]]
applyMoves = scanl moveRope

moveRope :: [Rope] -> Point -> [Rope]
moveRope [] _ = []
moveRope [Rope h t] step = [Rope movedHead movedTail] where
    movedHead = movePoint h step
    movedTail = moveTailIfNeeded t movedHead
moveRope ((Rope h t):(Rope hn tn):rs) step = Rope movedHead movedTail:moveRope (Rope movedTail tn:rs) (Point 0 0) where
    movedHead = movePoint h step
    movedTail = moveTailIfNeeded t movedHead

movePoint :: Point -> Point -> Point
movePoint (Point x1 y1) (Point x2 y2) = Point (x1+x2) (y1+y2)

getSteps :: Move -> [Point]
getSteps (MoveRight d) = replicate d $ Point 1 0
getSteps (MoveLeft d) = replicate d $ Point (-1) 0
getSteps (MoveUp d) = replicate d $ Point 0 1
getSteps (MoveDown d) = replicate d $ Point 0 (-1)

moveTailIfNeeded :: Point -> Point -> Point
moveTailIfNeeded (Point tx ty) (Point hx hy) = Point (tx + moveX) (ty + moveY) where
    (dx, dy) = (hx - tx, hy - ty)
    (moveX, moveY) = if (abs dx + abs dy) > 2 then (signum dx, signum dy) else
        (if abs dx > 1 then signum dx else 0,
         if abs dy > 1 then signum dy else 0)
