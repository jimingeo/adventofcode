-- Day 4: Overlapping work assignment
main = do
    content <- getContents
    let workAssignments = map parseWorkAssignment (lines content)
    putStrLn "Day 4, part 1 solution:"
    print $ length $ filter isContainedWork workAssignments
    putStrLn "Day 4, part 2 solution:"
    print $ length $ filter isWorkOverlapping workAssignments

parseWorkAssignment :: String -> ((Int, Int), (Int, Int))
parseWorkAssignment inputLine =
    (parseWork elf1, parseWork elf2) where
        elf1 = takeWhile (/= ',') inputLine
        elf2 = tail $ dropWhile (/= ',') inputLine
        parseWork work = (read (takeWhile (/= '-') work)::Int,
                          read (tail $ dropWhile (/= '-') work)::Int)

isContainedWork :: ((Int, Int), (Int, Int)) -> Bool
isContainedWork (elf1, elf2) =
    isWithin elf1 elf2 || isWithin elf2 elf1 where
        isWithin (start1, end1) (start2, end2) =
            start1 <= start2 && end1 >= end2

isWorkOverlapping :: ((Int, Int), (Int, Int)) -> Bool
isWorkOverlapping (elf1, elf2) =
    isOverlapping elf1 elf2 || isOverlapping elf2 elf1 where
        isOverlapping (start1, end1) (start2, end2) =
            (start1 <= start2 && end1 >= start2) || (start1 <= end2 && end1 >= end2)
