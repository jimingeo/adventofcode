data Move = Rock | Paper | Scissors deriving Show

data Game = Game {
    opponentMove::Move,
    myMove::Move
} deriving Show

data Result = Win | Loss | Draw deriving (Show, Eq)

main = do
    contents <- getContents
    let games = map translateToGame (lines contents)
    putStrLn "Day 2, part 1 solution:"
    print $ foldr getPointsForGame 0 games
    let newGames = map translateNewGameRules (lines contents)
    putStrLn "Day 2, part 2 solution:"
    print $ foldr getPointsForGame 0 newGames

translateToGame :: String -> Game
translateToGame [opp, _, me] = Game (translateOppMove opp) (translateMyMove me)

translateOppMove 'A' = Rock
translateOppMove 'B' = Paper
translateOppMove 'C' = Scissors

translateMyMove 'X' = Rock
translateMyMove 'Y' = Paper
translateMyMove 'Z' = Scissors

getPointsForMove :: Move -> Int
getPointsForMove Rock = 1
getPointsForMove Paper = 2
getPointsForMove Scissors = 3

determineResult :: Game -> Result
determineResult (Game Rock Paper) = Win
determineResult (Game Rock Scissors) = Loss
determineResult (Game Paper Rock) = Loss
determineResult (Game Paper Scissors) = Win
determineResult (Game Scissors Rock) = Win
determineResult (Game Scissors Paper) = Loss
determineResult _ = Draw

getPointForOutcome :: Game -> Int
getPointForOutcome = pointForResult . determineResult where
    pointForResult Win = 6
    pointForResult Loss = 0
    pointForResult Draw = 3

getPointsForGame :: Game -> Int -> Int
getPointsForGame game acc = acc + (getPointsForMove . myMove) game + getPointForOutcome game

translateToDesiredOutcome :: Char -> Result
translateToDesiredOutcome 'X' = Loss
translateToDesiredOutcome 'Y' = Draw
translateToDesiredOutcome 'Z' = Win

translateNewGameRules :: String -> Game
translateNewGameRules [opp, _, me] =
    Game opponentMove
        (getMoveForDesiredOutcome opponentMove $ translateToDesiredOutcome me) where
        opponentMove = translateOppMove opp

getMoveForDesiredOutcome :: Move -> Result -> Move
getMoveForDesiredOutcome opponentMove desiredOutcome
    | determineResult (Game opponentMove Rock) == desiredOutcome = Rock
    | determineResult (Game opponentMove Paper) == desiredOutcome = Paper
    | determineResult (Game opponentMove Scissors) == desiredOutcome = Scissors
