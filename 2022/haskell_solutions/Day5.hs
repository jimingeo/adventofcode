-- Day 5: Crate reorganization with giant cargo crane
data CraneOperationProcedure = CraneOperationProcedure {
    numberOfCrates::Int,
    fromStack::Int,
    toStack::Int
} deriving Show

main = do
    content <- getContents
    let crateArrangement = parseCrateStacks (takeWhile (/= "") (lines content))
    let rearrangementProcedure = map parseRearrangementProcedure ((tail . dropWhile (/= "")) (lines content))
    putStrLn "Day 5, part 1:"
    print crateArrangement

parseCrateStacks :: [String] -> [String]
parseCrateStacks content =
    (tail . reverse) firstStack:parseCrateStacks restStack where
        firstStack = map (head . tail . take 3) content
        restStack = map (drop 4) content

parseRearrangementProcedure :: String -> CraneOperationProcedure
parseRearrangementProcedure line =
    CraneOperationProcedure (read (proc!!1)::Int) (read (proc!!3)::Int) (read (proc!!5)::Int) where
        proc = words line
