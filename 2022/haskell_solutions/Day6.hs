-- Day 6: Communication system signal decoder

main = do
    content <- getContents
    putStrLn "Day 6, part 1 solution:"
    print $ findStartOfPacket content
    putStrLn "Day 6, part 2 solution:"
    print $ findStartOfMessage content

findStartOfPacket :: String -> Int
findStartOfPacket packet
    | areElementsUnique (take 4 packet) = 4
    | otherwise = 1 + findStartOfPacket (tail packet)

findStartOfMessage :: String -> Int
findStartOfMessage packet
    | areElementsUnique (take 14 packet) = 14
    | otherwise = 1 + findStartOfMessage (tail packet)

areElementsUnique :: String -> Bool
areElementsUnique [x] = True    -- Single element is unique
areElementsUnique (x:xs)
    | x `elem` xs = False
    | otherwise = areElementsUnique xs
