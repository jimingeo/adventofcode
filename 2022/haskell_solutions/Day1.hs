import Data.List(sort)
main = do
    content <- getContents
    let sorted_elves_calories = (reverse . sort) $ parseInput (lines content) []
    putStrLn "Day 1, part 1: Highest calorie"
    print $ head sorted_elves_calories
    putStrLn "Day 1, part 2: Sum of highest three calories"
    print $ (sum . take 3) sorted_elves_calories

-- Read a list of calorie per snack, separated by blank line for each elf, and return a list of
-- total calories per elf.
parseInput :: [String] -> [Int] -> [Int]
parseInput [] y = y
parseInput ("":x:xs) y = parseInput xs ((read x::Int):y)
parseInput (x:xs) [] = parseInput xs [read x::Int]
parseInput (x:xs) (y:ys) = parseInput xs (((read x::Int) + y):ys)
