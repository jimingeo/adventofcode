Solutions for AoC 2021 from https://adventofcode.com/2021

**Environment setup:**
The execution environment is configured with a simple Nix config to setup the suitable haskell
environment, along with necessary supporting packages. Start the environment by running:
```shell
# nix-shell
```

**Running the programs:**
The input files for each day is stored under `inputs/day<day>.txt`. The programs expect inputs from
stdin, so run the program by piping the file contents to stdin. For example:
```shell
# cat inputs/day1.txt | runhaskell Day1.hs
```
(or)
```shell
# runhaskell Day1.hs < inputs/day1.txt
```
