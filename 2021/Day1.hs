-- Solution for Day1
-- Program takes the list of depths as a multiline depths through stdin
-- For part 1, the output will be the number of times the depth increases.
-- For part 2, the output will be the number of times the total depth of three contiguous depth
-- readings increases

main = do
    content <- getContents
    let depths = map (read::String->Int) (lines content)
    putStrLn "Part 1 - Increasing depth count:"
    print $ length $ filter (>0) (getDepthGradient depths)
    putStrLn "Part 2 - Increasing depth triples count:"
    print $ length $ filter (>0) (getDepthGradient . getSumOfTriplesDepth $ depths)

getDepthGradient :: [Int] -> [Int]
-- Pair up every depth with the next one, and find the difference between each pair
getDepthGradient depths = zipWith subtract depths (tail depths)

getSumOfTriplesDepth :: [Int] -> [Int]
-- Calculate a list of sums of every three contiguous depth readings
getSumOfTriplesDepth depths =
    zipWith3 (\x y z -> x + y + z) depths (tail depths) (tail $ tail depths)
