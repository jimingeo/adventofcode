-- Solution for Day4
-- Program takes the list of random bingo numbers and a random set of bingo boards (5x5) as a multiline input through stdin
-- For part 1, the output will be the product of the sum of unmarked boxes of the first winning board and the
-- winning number.
-- For part 2, the output will be the product of the sum of the unmarked boxes of the LAST winning
-- board and the winning number.

data Cell = Cell {
    item :: String,
    marked :: Bool
} deriving (Show, Ord, Eq)

main = do
    content <- getContents
    let bingoData = lines content
    let bingoNums = splitNums $ head bingoData
    let bingoBoards = parseBoards $ drop 2 bingoData -- Drop nums and the empty line
    let (winningNo, winningBoard) = playBingo bingoNums bingoBoards
    putStrLn "Part 1 - Winning combination:"
    print $ sum (flattenToInt winningBoard) * (read winningNo::Int)
    let (lastWinningNo, lastWinningBoard) = playBingoTillLastBoard bingoNums bingoBoards
    putStrLn "Part 2 - Last winning combination:"
    print $ sum (flattenToInt lastWinningBoard) * (read lastWinningNo::Int)

splitNums :: String -> [String]
splitNums = foldr f [[]] where
    f c l@(x:xs) | c == ',' = []:l
                 | otherwise = (c:x):xs

-- Return a 3-dimensional array containing bingo boards, each 5x5
parseBoards :: [String] -> [[[Cell]]]
parseBoards [] = [[]]
parseBoards (x:xs)
    | x == "" = []: parseBoards xs
    | otherwise = (map (`Cell` False) (words x) : head res) : tail res where res = parseBoards xs

-- Play bingo against each board with input numbers till a winning board is found
playBingo :: [String] -> [[[Cell]]] -> (String, [[Cell]])
playBingo [] _ = ("", [])
playBingo (x:xs) boards = checkWinner $ map (callNum x) boards where
    checkWinner b = if not (any checkWinningBoard b) then playBingo xs b else (x, head (filter checkWinningBoard b))

-- Call out a number, by marking the number if present on the board
callNum :: String -> [[Cell]] -> [[Cell]]
callNum n = map (map (\c@(Cell item _) -> if item == n then Cell item True else c))

-- Check if a board is a winning board
checkWinningBoard :: [[Cell]] -> Bool
checkWinningBoard board = rowComplete board || columnComplete board where
    rowComplete [] = False
    rowComplete (b:bs) = all marked b || rowComplete bs
    columnComplete b
        | [] `elem` b = False -- if the elements are empty
        | otherwise = all (marked . head) b || columnComplete (map tail b)

flattenToInt :: [[Cell]] -> [Int]
flattenToInt board = [read (item s) :: Int | row <- board, s <- row, not (marked s)]

-- Find the last winning board. Assuming that all boards will eventually finish; in other words, this is a fair game.
playBingoTillLastBoard :: [String] -> [[[Cell]]] -> (String, [[Cell]])
playBingoTillLastBoard _ [] = ("", [])
playBingoTillLastBoard nums [x] = playBingo nums [x]
playBingoTillLastBoard (x:xs) boards =
    playBingoTillLastBoard xs (filter (not . checkWinningBoard) (map (callNum x) boards))
