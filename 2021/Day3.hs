-- Solution for Day3
-- Program takes a list of submarine binary diagnostics data as a multiline stdin input
-- We assume that there are no errors in the input for simplification.
-- For part 1, output will be the power consumption, which is product of the gamma and epsilon rates
-- For part 2, output will be the life support rating, which is the product of Oxygen generator
-- rating and the CO2 scrubber rating.

import Data.Char(digitToInt)

main = do
    content <- getContents
    let binaryCodes = lines content
    let mcb = getMCB binaryCodes
    putStrLn "Part 1 - Power consumption (product of gamma and epsilon): "
    print $ convertBinaryToDecimal mcb * (convertBinaryToDecimal . complement $ mcb)
    putStrLn "Part 2 - Life support rating (product of O2 and CO2 rating):"
    let o2Rating = convertBinaryToDecimal . calculateLifeSupportSystemRating o2GeneratorCriteria $ binaryCodes
    let co2Rating = convertBinaryToDecimal . calculateLifeSupportSystemRating co2ScrubberCriteria $ binaryCodes
    print $ o2Rating * co2Rating

-- For a list of binary strings, get the binary string containing the most common bits for each
-- position.
getMCB :: [String] -> String
getMCB binaryNumbers =
    zipWith (\zeros ones -> if ones >= zeros then '1' else '0')
        (countZeros binaryNumbers) (countOnes binaryNumbers)

-- For a list of binary strings, get the binary string containing the least common bits for each
-- position.
getLCB :: [String] -> String
getLCB binaryNumbers =
    zipWith (\zeros ones -> if zeros <= ones then '0' else '1')
        (countZeros binaryNumbers) (countOnes binaryNumbers)

complement :: String -> String
complement = map (\x -> if x == '1' then '0' else '1')

-- Return a list containing the number of ones in each binary position for a list of binary numbers
countOnes :: [String] -> [Int]
countOnes binaryNumbers = foldl onesCounter counter binaryNumbers where
    onesCounter currentCount binary =
        zipWith (\binDigit count -> if binDigit == '1' then count + 1 else count) binary currentCount
    counter = replicate (length . head $ binaryNumbers) 0

-- Return a list containing the number of zeros in each binary position for a list of binary numbers
countZeros :: [String] -> [Int]
countZeros binaryNumbers = foldl zerosCounter counter binaryNumbers where
    zerosCounter currentCount binary =
        zipWith (\binDigit count -> if binDigit == '0' then count + 1 else count) binary currentCount
    counter = replicate (length . head $ binaryNumbers) 0

-- Convert a binary string to a decimal integer
convertBinaryToDecimal :: String -> Int
convertBinaryToDecimal b = fst $ foldr (\n (acc, multiplier) -> (acc + digitToInt n * multiplier, multiplier * 2)) (0, 1) b

-- Life support system calculator
calculateLifeSupportSystemRating :: ([String] -> Int -> [String]) -> [String] -> String
calculateLifeSupportSystemRating func = tryTillOneValue 0 where
    tryTillOneValue _ [x] = x
    tryTillOneValue pos binaryNumbers = tryTillOneValue (pos + 1) (func binaryNumbers pos)

-- Filtering criteria for O2 Generator
o2GeneratorCriteria :: [String] -> Int -> [String]
o2GeneratorCriteria binaryNumbers pos =
    filter (\bin -> bin !! pos == mcb !! pos) binaryNumbers where
        mcb = getMCB binaryNumbers

-- Filtering criteria for CO2 scrubber
co2ScrubberCriteria :: [String] -> Int -> [String]
co2ScrubberCriteria binaryNumbers pos =
    filter (\bin -> bin !! pos == lcb !! pos) binaryNumbers where
        lcb = getLCB binaryNumbers
