-- Solution for Day2
-- Program takes a list of submarine commands as a multiline stdin input
-- We assume that there are no errors in the input for simplification.
-- For part 1, output will be the product of the final horizontal position and depth
-- For part 2, the output will again be the product of the final horizontal position and depth,
-- after applying the revised navigation logic

data Instruction = Forward Int | Down Int | Up Int deriving Show

main = do
    content <- getContents
    let instructions = lines content
    let (dist, depth) = executeInstructions $ map parseInstruction instructions
    putStrLn "Part 1 - Product of distance and depth:"
    print (dist * depth)
    let (dist, depth) = executeRevisedInstructions $ map parseInstruction instructions
    putStrLn "Part 2 - Product of distance and depth:"
    print (dist * depth)

-- Translate the text instructions to `Instruction` type
parseInstruction :: String -> Instruction
parseInstruction instruction = parse (words instruction)
    where parse ["forward", x] = Forward (read x::Int)
          parse ["down", x] = Down (read x::Int)
          parse ["up", x] = Up (read x::Int)

-- Apply a submarine command on a set of coordinates (horizontal pos, depth)
applyInstruction :: (Int, Int) -> Instruction -> (Int, Int)
applyInstruction (x,y) (Forward n) = (x+n, y)
applyInstruction (x,y) (Down n) = (x, y+n)
applyInstruction (x,y) (Up n) = (x, y-n)

-- Execute the sequence of instructions to get the final coordinate of the submarine
executeInstructions :: [Instruction] -> (Int, Int)
executeInstructions = foldl applyInstruction (0, 0)

-- Apply the revised submarine commands on the submarine state (horizontal position, depth, aim)
applyRevisedInstruction :: (Int, Int, Int) -> Instruction -> (Int, Int, Int)
applyRevisedInstruction (x,y,aim) (Forward n) = (x+n, y+(n*aim), aim)
applyRevisedInstruction (x,y,aim) (Down n) = (x, y, aim+n)
applyRevisedInstruction (x,y,aim) (Up n) = (x, y, aim-n)

-- Execute the new sequence of instructions to get the final coordinates
executeRevisedInstructions :: [Instruction] -> (Int, Int)
executeRevisedInstructions instructions = (x, y)
    where (x,y,_) = foldl applyRevisedInstruction (0, 0, 0) instructions
