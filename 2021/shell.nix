# Minimal nix configuration
let
    myNixPkgs = import <nixpkgs> {};
in
    myNixPkgs.mkShell {
        nativeBuildInputs = with myNixPkgs; [
            entr ghc haskellPackages.hlint
        ];
    }
