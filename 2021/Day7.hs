-- Solution for Day 7 (Crab submarine)
-- Program for Crab submarine guidance system
-- For part 1, the output will be the least fuel cost to align all crab submarines
-- For part 2, the output will be the least fuel cost to align all crab submarines based on the
-- revised fuel cost model.

import Data.Char(digitToInt)

main = do
    content <- getContents
    let crabSubPositions = parseCrabSubPosition content
    putStrLn "Part 1: Minimum fuel to align crab subs: "
    print $ minimum $ map (calculateCostToPosition crabSubPositions) crabSubPositions
    putStrLn "Part 2: Minimum fuel to align crab subs with revised model: "
    print $ minimum $ map (calculateRevisedCostToPosition crabSubPositions) [0..(maximum crabSubPositions)]

parseCrabSubPosition :: String -> [Int]
parseCrabSubPosition "" = []
parseCrabSubPosition "\n" = []  -- Special case to trap end of line
parseCrabSubPosition (',':inp) = parseCrabSubPosition inp
parseCrabSubPosition inp =
    (read (takeWhile (/= ',') inp)::Int) : parseCrabSubPosition (dropWhile (/= ',') inp)

calculateCostToPosition :: [Int] -> Int -> Int
calculateCostToPosition subPositions pos =
    sum $ map (abs . subtract pos) subPositions

calculateRevisedCostToPosition :: [Int] -> Int -> Int
calculateRevisedCostToPosition subPositions pos =
    sum $ map costToPos subPositions where
        costToPos p = sum [1..(abs . subtract pos) p]
