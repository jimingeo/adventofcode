-- Solution for Day5
-- Program takes the list of hydrothermal vent lines as a multiline input through stdin
-- For part 1, the output will be the number of overlapping horizontal or vertical lines.
-- For part 2, the output will be the number of overlapping all overlapping points.
-- @TODO: This is a brute-force implementation that takes quite a long time to run for big inputs.
-- Scope for optimization.

import qualified Data.Set as Set

data Point = Point Int Int deriving (Show, Eq, Ord)

main = do
    content <- getContents
    let ventLines = map parseVentLine (lines content)
    let straightPoints = flattenPoints $ [generatePointsOfLine line | line <- ventLines, isHorizontalLine line || isVerticalLine line]
    putStrLn "Part 1 - Overlapping horizontal and vertical lines:"
    print $ Set.size $ repeatingPoints straightPoints
    let points = flattenPoints $ map generatePointsOfLine ventLines
    putStrLn "Part 2 - All overlapping points:"
    print $ Set.size $ repeatingPoints points

parseVentLine :: String -> (Point, Point)
parseVentLine line = (toPoint fromPt, toPoint toPt) where
    pointStr = words line
    fromPt = splitPoints (head pointStr)
    toPt = splitPoints (pointStr !! 2)
    toPoint (x, y) = Point (read x::Int) (read y::Int)

splitPoints :: String -> (String, String)
splitPoints pt = (takeWhile (/= ',') pt, (tail . dropWhile (/= ',')) pt)

isHorizontalLine :: (Point, Point) -> Bool
isHorizontalLine (Point x1 y1, Point x2 y2) = y1 == y2

isVerticalLine :: (Point, Point) -> Bool
isVerticalLine (Point x1 y1, Point x2 y2) = x1 == x2

generatePointsOfLine :: (Point, Point) -> [Point]
generatePointsOfLine (p1@(Point x1 y1), p2@(Point x2 y2))
    | isHorizontalLine (p1, p2) =
    map (`Point` y1) (getPointsOnAxis x1 x2)
    | isVerticalLine (p1, p2) =
    map (Point x1) (getPointsOnAxis y1 y2)
    | otherwise =
    zipWith Point (getPointsOnAxis x1 x2) (getPointsOnAxis y1 y2)
    where
        getPointsOnAxis start end
            | start < end = [start..end]
            | otherwise = [start, start-1..end]

repeatingPoints :: [Point] -> Set.Set Point
repeatingPoints [] = Set.empty
repeatingPoints (x:xs)
    | x `elem` xs = Set.union (Set.singleton x) (repeatingPoints xs)
    | otherwise = repeatingPoints xs

flattenPoints :: [[Point]] -> [Point]
flattenPoints points = [point | line <- points, point <- line]
