-- Solution for Day 6 (Lanternfish simulator)
-- Program to simulate the lanternfish population
-- For part 1, the output will have the lanternfish count at the end of 80 days.
-- For part 2, the output will have the lanternfish count at the end of 256 days.
import Data.Char(digitToInt)

newtype FishProductionDayMap = FishProductionDayMap [Int] deriving Show

main = do
    content <- getContents
    let lanternfishMap = fishesPerInternalTimer (parseFish content) (FishProductionDayMap $ replicate 9 0)
    putStrLn "Part 1: Number of lanternfish at the end of 80 days"
    print $ countFish $ iterate simulateNextDay lanternfishMap !! 80
    print lanternfishMap
    putStrLn "Part 2: Number of lanternfish at the end of 256 days"
    print $ countFish $ iterate simulateNextDay lanternfishMap !! 256

parseFish :: String -> [Int]
parseFish [] = []
parseFish "\n" = []  -- Special case to trap end of line
parseFish (x:xs)
    | x == ',' = parseFish xs
    | otherwise = digitToInt x : parseFish xs

fishesPerInternalTimer :: [Int] -> FishProductionDayMap -> FishProductionDayMap
fishesPerInternalTimer fishes (FishProductionDayMap counters) =
    FishProductionDayMap $ [count i (counters !! i) | i <- [0..(length counters - 1)]] where
    count n c = c + (length . filter (== n)) fishes

simulateNextDay :: FishProductionDayMap -> FishProductionDayMap
simulateNextDay (FishProductionDayMap fishMap) = FishProductionDayMap (cycleFishMap fishMap)

cycleFishMap :: [Int] -> [Int]
cycleFishMap fishMap = realignMap ((take (length fishMap) . drop 1 . cycle) fishMap)

realignMap :: [Int] -> [Int]
realignMap [] = []
realignMap [fm3, fm2, fm1] = fm3 + fm1: fm2: [fm1]
realignMap (f:fs) = f : realignMap fs

countFish :: FishProductionDayMap -> Int
countFish (FishProductionDayMap fishMap) = sum fishMap
