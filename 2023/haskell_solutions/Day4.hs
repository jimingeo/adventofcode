-- Day 4: Scratchcards

data Card = Card {
    cardNo::Int,
    winningNumbers::[Int],
    playerNumbers::[Int],
    wins::[Int]
} deriving Show

main = do
    content <- getContents
    let cards = map parseCards (lines content)
    putStrLn "Day 4, part 1: scratchcard total points:"
    print $ sum $ map cardPoints cards
    putStrLn "Day 4, part 2: total cards won, new rules:"
    print $ length $ winningCards cards

parseCards :: String -> Card
parseCards line = Card card winNos playerNos wins
    where [cardLabel, cardContent] = splitAtChar ':' line
          card = (read . last . splitAtChar ' ') cardLabel
          winNos = map read ((words . head . splitAtChar '|') cardContent)
          playerNos = map read ((words . last . splitAtChar '|') cardContent)
          wins = [p | p <- playerNos, p `elem` winNos]

cardPoints :: Card -> Int
cardPoints card@(Card _ winners plays wins) =
    if null wins then 0 else 2 ^ (length wins - 1)

winningCards :: [Card] -> [Card]
winningCards [] = []
winningCards cards@(c:cs) =
    let wonStack = getWinningCards cards ++ winningCards cs
    in
    wonStack

getWinningCards :: [Card] -> [Card]
getWinningCards [] = []
getWinningCards (c@(Card _ _ _ wins):cs) =
    let winCount = length wins
        wonCards = take winCount cs
        wonStack = concat [getWinningCards (wc:drop n cs) | (wc, n) <- zip wonCards [1..]]
    in c:wonStack

-- Helper functions

splitAtChar :: Char -> String -> [String]
splitAtChar _ "" = []
splitAtChar c (' ':ss) = splitAtChar c ss
splitAtChar c str@(s:ss) =
    if s == c then
        splitAtChar c ss
    else
        takeWhile (/= c) str: splitAtChar c (dropWhile (/= c) str)
