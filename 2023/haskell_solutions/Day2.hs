-- Day 2: Cube probability game

newtype Red = Red Int deriving Show
newtype Green = Green Int deriving Show
newtype Blue = Blue Int deriving Show

data Game = Game {
    gameId::Int,
    cubeCounts::[(Red, Green, Blue)]
} deriving Show

main = do
    content <- getContents
    let games = map parseLine (lines content)
    putStrLn "Day 2, part 1: possible games"
    print $ addGameIds $ getPossibleGames (Red 12, Green 13, Blue 14) games
    putStrLn "Day 2, part 2: sum of power of cube sets"
    print $ sum $ map powerOfCubeSet games

getPossibleGames :: (Red, Green, Blue) -> [Game] -> [Game]
getPossibleGames gameInst = filter (isGamePossible gameInst)

isGamePossible :: (Red, Green, Blue) -> Game -> Bool
isGamePossible (Red r, Green g, Blue b) (Game _ plays) = all validPlay plays
    where validPlay (Red pr, Green pg, Blue pb) = pr <= r && pb <= b && pg <= g

addGameIds :: [Game] -> Int
addGameIds = foldr (\game sum -> sum + gameId game) 0

powerOfCubeSet :: Game -> Int
powerOfCubeSet = power . minCubesRequired
    where power (Red r, Green g, Blue b) = r * g * b

minCubesRequired :: Game -> (Red, Green, Blue)
minCubesRequired (Game _ plays) =
    foldr (\(Red r, Green g, Blue b)
            (Red minr, Green ming, Blue minb) ->
                (Red (max r minr), Green (max g ming), Blue (max b minb)))
            (Red 0, Green 0, Blue 0) plays

-- Helper functions
parseLine :: String -> Game
parseLine str =
    Game (parseGameId gameIdStr) (parseGame gameData)
    where [gameIdStr, gameData] = splitAtChar ':' str

parseGameId :: String -> Int
parseGameId ('G':'a':'m':'e':' ':x) = read x::Int

parseGame :: String -> [(Red, Green, Blue)]
parseGame "" = []
parseGame ss = map parseOneInstance (splitAtChar ';' ss)

parseOneInstance :: String -> (Red, Green, Blue)
parseOneInstance ss = (Red red, Green green, Blue blue)
    where tokens = splitAtChar ',' ss
          isColorToken t color = (last . splitAtChar ' ') t == color
          getColor [] _ = 0
          getColor (t:ts) color =
            if isColorToken t color then
                read ((head . splitAtChar ' ') t)::Int
            else
                getColor ts color
          red = getColor tokens "red"
          green = getColor tokens "green"
          blue = getColor tokens "blue"

splitAtChar :: Char -> String -> [String]
splitAtChar _ "" = []
splitAtChar c (' ':ss) = splitAtChar c ss
splitAtChar c str@(s:ss) =
    if s == c then
        splitAtChar c ss
    else
        takeWhile (/= c) str: splitAtChar c (dropWhile (/= c) str)
