-- Day 6: Wait for it

main = do
    content <- getContents
    let [time, dist] = map parseInput (lines content)
    let possibleDistances = map distanceDistribution time
    print "Day 6, part 1: number of winning possibilities"
    print $ product [length $ winningRaces d wins |
                 (d, wins) <- zip dist possibleDistances]
    let [fullTime, fullDist] = map ((read::String -> Int) . concat . tail . words) (lines content)
    let newPossibleDistances = distanceDistribution fullTime
    print "Day 6, part 2: number of winning possibilities"
    print $ length $ winningRaces fullDist newPossibleDistances

distanceDistribution :: Int -> [Int]
distanceDistribution time = [travelledDistance chargeTime remainingTime |
                             (chargeTime, remainingTime) <- timeDistribution]
    where timeDistribution = [(x, time - x) | x <- [1..time-1]]
          travelledDistance speed t = speed * t

winningRaces :: Int -> [Int] -> [Int]
winningRaces recordDistance = filter (> recordDistance)

-- Helper functions
parseInput :: String -> [Int]
parseInput line = map (read::String->Int) (tail $ words line)
