-- Day 7: Camel cards
import Data.List (sort, group)

data Card = A | K | Q | J | T | C9 | C8 | C7 | C6 | C5 | C4 | C3 | C2 | Joker
    deriving (Show, Eq, Ord)

data HandType = FiveOfAKind
              | FourOfAKind
              | FullHouse
              | ThreeOfAKind
              | TwoPair
              | OnePair
              | HighCard
              deriving (Show, Eq, Ord)

data Hand = Hand {
    cards :: [Card],
    handType :: HandType
} deriving (Show, Eq)

instance Ord Hand where
    compare (Hand cards1 type1) (Hand cards2 type2)
        | type1 == type2 = compare cards1 cards2
        | otherwise = compare type1 type2

main = do
    content <- getContents
    let handBets = [(parseHand h, read bet::Int) | [h, bet] <- map words (lines content)]
    putStrLn "Day 7, part 1: Total winnings"
    print $ sum [snd bet * i | (bet, i) <- zip ((reverse . sort) handBets) [1..]]
    let handBetsWithJoker = [(applyJokerRule h, bet) | (h, bet) <- handBets]
    putStrLn "Day 7, part 2: Total winnings"
    print $ sum [snd bet * i | (bet, i) <- zip ((reverse . sort) handBetsWithJoker) [1..]]

determineHandType :: [Card] -> HandType
determineHandType cards
    | countSame == 1 = FiveOfAKind
    | countSame == 5 = HighCard
    | countGroupOfLen 4 == 1 = FourOfAKind
    | countGroupOfLen 3 == 1 =
        if countGroupOfLen 2 == 1 then FullHouse else ThreeOfAKind
    | countGroupOfLen 2 == 2 = TwoPair
    | countGroupOfLen 2 == 1 = OnePair
    where cardGroup = (group . sort) cards
          countSame = length cardGroup
          countGroupOfLen l = length (filter ((== l) . length) cardGroup)

applyJokerRule :: Hand -> Hand
applyJokerRule hand@(Hand cards cardType)
    | cardType == FiveOfAKind = Hand convertedCards cardType
    | Joker `elem` convertedCards = Hand convertedCards (determineBestHandType convertedCards)
    | otherwise = hand
    where convertedCards = replaceCard J Joker cards

replaceCard :: Card -> Card -> [Card] -> [Card]
replaceCard from to cards = [if c == from then to else c | c <- cards]

determineBestHandType :: [Card] -> HandType
determineBestHandType cards = minimum possibleHandTypes
    where
        possibleHands = [replaceCard Joker c cards | c <- cards, c /= Joker]
        possibleHandTypes = map determineHandType possibleHands

-- Helper functions
parseHand :: String -> Hand
parseHand s = Hand cards (determineHandType cards)
    where cards = parseCards s

parseCards :: String -> [Card]
parseCards [] = []
parseCards (c:cs) = toCard c:parseCards cs where
    toCard card = case card of
        'A' -> A
        'K' -> K
        'Q' -> Q
        'J' -> J
        'T' -> T
        '9' -> C9
        '8' -> C8
        '7' -> C7
        '6' -> C6
        '5' -> C5
        '4' -> C4
        '3' -> C3
        '2' -> C2
