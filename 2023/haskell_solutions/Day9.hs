-- Day 9: Mirage Maintainence

main = do
    content <- getContents
    let oasisData = map (parseLine . words) (lines content)
    let extrapolation = map (extrapolateReadings . reduceTillZeroes) oasisData
    putStrLn "Day 9, part 1: Sum of extrapolated values"
    print $ sum $ map last extrapolation
    let historicExtrapolation = map (extrapolateHistory . reduceTillZeroes) oasisData
    putStrLn "Day 9, part 2: Sum of historic extrapolated values"
    print $ sum $ map head historicExtrapolation

reduceTillZeroes :: [Int] -> [[Int]]
reduceTillZeroes ns
    | all (== 0) ns = [ns]
    | otherwise = ns:reduceTillZeroes (zipWith (flip (-)) ns (tail ns))

extrapolateReadings :: [[Int]] -> [Int]
extrapolateReadings (n:ns)
    | all (== 0) n = n ++ [0]
    | otherwise = n ++ [last n + last extrapolatedNS]
    where extrapolatedNS = extrapolateReadings ns

extrapolateHistory :: [[Int]] -> [Int]
extrapolateHistory (n:ns)
    | all (== 0) n = 0:n
    | otherwise = (head n - head extrapolatedNS):n
    where extrapolatedNS = extrapolateHistory ns

-- Helper functions
parseLine :: [String] -> [Int]
parseLine = map (read::String -> Int)
