-- Day 3: Gear ratios
import Data.Char (isDigit)

type Position = (Int, Int)
type Schematic = [String]

main = do
    content <- getContents
    let schematic = lines content
    putStrLn "Day 3, part 1: sum of engine part numbers"
    print $ sumOfPartNumbers schematic
    putStrLn "Day 3, part 2: gear ratios"
    print $ sum $ getGearRatios schematic

sumOfPartNumbers :: Schematic -> Int
sumOfPartNumbers schematic =
    foldr (\(n, _) acc -> acc + n) 0 (getNumbersWithAdjacentSymbols schematic)

getNumbersWithAdjacentSymbols :: Schematic -> [(Int, [Position])]
getNumbersWithAdjacentSymbols schematic =
    let numbersWithPos = getNumbers schematic 0
        numberAdjacentPos (n, ps) =
            (n, concat [getAdjacentPositions schematic pos | pos <- ps])
        numbersWithAdjPos = map numberAdjacentPos numbersWithPos
    in 
        filter (\(n, ps) ->
            foldr (\p acc -> isElemSymbol schematic p || acc) False ps)
            numbersWithAdjPos

getGearRatios :: Schematic -> [Int]
getGearRatios schematic = 
    map (\[(n1, _), (n2, _)] -> n1 * n2) (getGearNumbers schematic)

getGearNumbers :: Schematic -> [[(Int, [Position])]]
getGearNumbers schematic =
    let numsWithAdjSymbols = getNumbersWithAdjacentSymbols schematic
        gears = findGears schematic
    in
        map (numsAdjacentToPosition numsWithAdjSymbols) gears

findGears :: Schematic -> [Position]
findGears schematic = 
    let numsWithAdjSymbols = getNumbersWithAdjacentSymbols schematic
        possibleGears = findSymbol schematic '*' 0
    in
        [gear | gear <- possibleGears,
            length (numsAdjacentToPosition numsWithAdjSymbols gear) == 2]
        
-- Helper functions
getAdjacentPositions :: Schematic -> Position -> [Position]
getAdjacentPositions schematic (x, y) =
    [(px, py) |
        px <- [x-1..x+1],
        px >= 0, px < (length . head) schematic,
        py <- [y-1..y+1],
        py >= 0, py < length schematic,
        (px, py) /= (x, y)]

getElement :: Schematic -> Position -> Char
getElement schematic (x, y) = schematic!!y!!x

isElemDigit :: Schematic -> Position -> Bool
isElemDigit schematic = isDigit . getElement schematic

isElemSymbol :: Schematic -> Position -> Bool
isElemSymbol schematic = isSymbol' . getElement schematic

isSymbol' :: Char -> Bool
isSymbol' e = (not . isDigit) e && e /= '.'

getNumbers :: Schematic -> Int -> [(Int, [Position])]
getNumbers [] _ = []
getNumbers (s:ss) column = numbersInRow ++ getNumbers ss (column + 1)
    where numbersInRow = map determinePositions (getNumbersInRow s 0)
          determinePositions (n, xs) = (n, zip xs (repeat column))

getNumbersInRow :: String -> Int -> [(Int, [Int])]
getNumbersInRow [] _ = []
getNumbersInRow row@(c:cs) fromCol
    | isDigit c =
        let num = takeWhile isDigit row
            len = length num
        in
        (read num, [fromCol..fromCol + len - 1]):getNumbersInRow (drop len row) (fromCol + len)
    | otherwise = getNumbersInRow cs (fromCol + 1)

findSymbol :: Schematic -> Char -> Int -> [Position]
findSymbol [] _ _ = []
findSymbol (s:ss) c column = zip xs (repeat column) ++ findSymbol ss c (column + 1)
    where xs = findSymbolInRow s 0
          findSymbolInRow [] _ = []
          findSymbolInRow (r:rs) row
            | c == r = row:findSymbolInRow rs (row + 1)
            | otherwise = findSymbolInRow rs (row + 1)

numsAdjacentToPosition :: [(Int, [Position])] -> Position -> [(Int, [Position])]
numsAdjacentToPosition nums pos =
    [num | num@(n, p) <- nums, pos `elem` p]
