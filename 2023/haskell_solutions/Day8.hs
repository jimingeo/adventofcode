-- Day 8: Haunted Wasteland

type Destination = (String, String)
type WastelandMap = [(String, Destination)]

main = do
    content <- getContents
    let directions = (head . lines) content
    let wastelandMap = map parseMap ((tail . tail . lines) content)
    putStrLn "Day 8, part 1: Steps from AAA to ZZZ"
    print $ length $ takeWhile ((/= "ZZZ") . fst) $ navigate wastelandMap "AAA" (cycle directions)
    let allStarts = map fst $ filter (endsIn 'A' . fst) wastelandMap
    let pathsToEnd = navigateInLockstep wastelandMap allStarts (cycle directions)
    putStrLn "Day 8, part 2: Steps till all paths end"
    print $ (length . tail . head) $ takeTillAllEnd pathsToEnd

navigateInLockstep :: WastelandMap -> [String] -> String -> [WastelandMap]
navigateInLockstep wasteland sources directions =
    [navigate wasteland s directions | s <- sources]

takeTillAllEnd :: [WastelandMap] -> [WastelandMap]
takeTillAllEnd wastelandMaps
    | all (endsIn 'Z' . fst) firstElems = [[e] | e <- firstElems]
    | otherwise = [f:rest | (f, rest) <- zip firstElems (takeTillAllEnd (map tail wastelandMaps))]
    where firstElems = map head wastelandMaps

navigate :: WastelandMap -> String -> String -> WastelandMap
navigate _ _ [] = []
navigate wasteland source (d:ds) = dest:navigate wasteland (dir d dest) ds
    where dest = lookupDest wasteland source
          dir 'L' = fst . snd
          dir 'R' = snd . snd

endsIn :: Char -> String ->Bool
endsIn c s = last s == c

lookupDest :: WastelandMap -> String -> (String, Destination)
lookupDest (w@(s, d):ws) source
    | s == source = w
    | otherwise = lookupDest ws source

-- Helper functions
parseMap :: String -> (String, Destination)
parseMap s = ((head . words) curr, (l, r))
    where [curr, dest] = splitAtChar '=' s
          [l, r] = splitAtChar ',' ((init . tail) dest)

splitAtChar :: Char -> String -> [String]
splitAtChar _ "" = []
splitAtChar c (' ':ss) = splitAtChar c ss
splitAtChar c str@(s:ss) =
    if s == c then
        splitAtChar c ss
    else
        takeWhile (/= c) str: splitAtChar c (dropWhile (/= c) str)
