-- Day 1: Decode calibration document
import Data.Char (isDigit)
main = do
    content <- getContents
    putStrLn "Day 1, part 1: Decoded calibration value"
    print $ (decodeCalibration . words) content
    putStrLn "Day 1, part 2: Corrected decoded calibration value"
    print $ (decodeCalibrationImproved . words) content

decodeCalibration :: [String] -> Int
decodeCalibration ss = sum $ map (read . decode) ss

decodeCalibrationImproved :: [String] -> Int
decodeCalibrationImproved ss = sum $ map (read . decode . translate) ss

-- Helper functions
decode :: String -> String
decode = firstAndLast . extractDigits

extractDigits :: String -> String
extractDigits = filter isDigit

firstAndLast :: String -> String
firstAndLast s = [head s, last s]

spelledNumToDigit :: String -> String
spelledNumToDigit s =
    case s of "one" -> "1"
              "two" -> "2"
              "three" -> "3"
              "four" -> "4"
              "five" -> "5"
              "six" -> "6"
              "seven" -> "7"
              "eight" -> "8"
              "nine" -> "9"
              _ -> s

startsWith :: String -> String -> Bool
startsWith searchStr subStr
    | subStr == take (length subStr) searchStr = True
    | otherwise = False

translate :: String -> String
translate [] = []
translate str@(x:xs)
    | null startingDigitStr = x:translate xs
    | otherwise = translate $ translateStartingWord str (head startingDigitStr)
    where strDigits = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
          startingDigitStr = filter (startsWith str) strDigits
          translateStartingWord ss digStr = spelledNumToDigit digStr ++ tail ss
