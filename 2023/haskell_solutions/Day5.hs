-- Day 5: If you give a seed a fertilizer

import Data.List (sort)

class Ord a => RangeClass a where
    outside :: a -> a -> Bool
    endsWithin :: a -> a -> Bool
    startsBefore :: a -> a -> Bool

data Range = Range {
    start::Int,
    end::Int -- exclusive
}

type Mapping = [(Range, Range)]

data TypedMap = TypedMap {
    source::String,
    dest::String,
    map_::Mapping
} deriving Show

instance Eq Range where
    r1 == r2 = start r1 == start r2 && end r1 == end r2

instance Ord Range where
    r1 < r2 = end r1 < start r2
    r1 > r2 = start r1 > end r2
    r1 <= r2 = end r1 <= start r2
    r1 >= r2 = start r1 >= end r2

instance RangeClass Range where
    (Range r1s r1e) `outside` (Range r2s r2e) = r1e <= r2s || r1s >= r2e
    (Range r1s r1e) `endsWithin` (Range r2s r2e) = r1e >= r2s && r1e <= r2e
    (Range r1s r1e) `startsBefore` (Range r2s r2e) = r1s < r2s

instance Show Range where
    show (Range start end) = "[" ++ show start ++ ".." ++ show end ++ ")"

main = do
    content <- getContents
    let seeds = (parseSeeds . head . lines) content
    let seedRange = map (`buildRange` 1) seeds
    let mappings = (parseMaps . tail . lines) content
    let translateToLocation = translateSourceRangeToDest mappings "seed" "location"
    let seedToLoc = map translateToLocation seedRange
    putStrLn "Day 5, part 1: closest location"
    print $ (start . minimum) $ map minimum seedToLoc
    let newSeedList = buildNewSeedList seeds
    let newSeedToLoc = map translateToLocation newSeedList
    putStrLn "Day 5, part 2: closest location for new seed list"
    print $ (start . minimum) $ map minimum newSeedToLoc

translateSourceRangeToDest :: [TypedMap] -> String -> String -> Range -> [Range]
translateSourceRangeToDest mappings source dest range
    | nextDest == dest = newVal
    | otherwise = concatMap (translateSourceRangeToDest mappings nextDest dest) newVal
    where sourceMap@(TypedMap nextSource nextDest mapping) = findMap mappings source
          newVal = sort $ lookupRange sourceMap range

findMap :: [TypedMap] -> String -> TypedMap
findMap (m@(TypedMap s _ _):ms) sourceType
    | s == sourceType = m
    | otherwise = findMap ms sourceType

lookupRange :: TypedMap -> Range -> [Range]
lookupRange (TypedMap _ _ mapping) = findValRange mapping

-- IMPORTANT: Assumes sorted Mapping
findValRange :: Mapping -> Range -> [Range]
findValRange [] range = [range]
findValRange mapping@((sourceRange, destRange):ms) range@(Range s e)
    | s == e = []
    | range `outside` sourceRange = findValRange ms range
    -- Range starts before current map but continues into the map
    | range `startsBefore` sourceRange =
        Range s (start sourceRange):
        findValRange mapping
            (buildRange (start sourceRange) (e - start sourceRange))
    -- Range ends within the current map
    | range `endsWithin` sourceRange = [buildRange (start destRange + delta) l]
    -- Range extends beyond the current map
    | otherwise = buildRange (start destRange + delta)
                        (end sourceRange - s):
                   findValRange ms
                        (buildRange (end sourceRange)
                            (l - (end sourceRange - s)))
    where l = e - s
          len = end sourceRange - start sourceRange
          delta = s - start sourceRange

buildNewSeedList :: [Int] -> [Range]
buildNewSeedList [] = []
buildNewSeedList (start:range:ss) = buildRange start range:buildNewSeedList ss

-- Helper functions
parseMaps :: [String] -> [TypedMap]
parseMaps [] = []
parseMaps ("":ls) = parseMaps ls
parseMaps lines =
    buildMap (takeWhile (/= "") lines):
    parseMaps (dropWhile (/= "") lines)

buildMap :: [String] -> TypedMap
buildMap (l:ls) = TypedMap sourceId destId (sort mapping)
    where [sourceId, _, destId] = (splitAtChar '-' . head . words) l
          tokens = map words ls
          mapping = [(buildRange (read sourceStart::Int) (read n::Int),
                      buildRange (read destStart::Int) (read n::Int)) |
                     [destStart, sourceStart, n] <- map words ls]

buildRange :: Int -> Int -> Range
-- Range end is exclusive
buildRange s l = Range s (s+l)

parseSeeds :: String -> [Int]
parseSeeds line = map (read::String->Int) ((words . last . splitAtChar ':') line)

splitAtChar :: Char -> String -> [String]
splitAtChar _ "" = []
splitAtChar c (' ':ss) = splitAtChar c ss
splitAtChar c str@(s:ss) =
    if s == c then
        splitAtChar c ss
    else
        takeWhile (/= c) str: splitAtChar c (dropWhile (/= c) str)
