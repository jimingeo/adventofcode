#!/usr/bin/env python
"""
AoC 2023 Day 1
"""

import sys

from pydantic import validate_arguments

STR_TO_DIGIT = {
    "one": '1',
    "two": '2',
    "three": '3',
    "four": '4',
    "five": '5',
    "six": '6',
    "seven": '7',
    "eight": '8',
    "nine": '9',
}

def read_input():
    """Read input data from stdin"""
    for line in sys.stdin:
        yield line.rstrip('\n')

@validate_arguments
def parse_calibration_values(inp: str) -> int:
    """Pick first and last digit of input line and combine them to return a 2-digit number"""
    digits = [c for c in inp if c.isdigit()]
    return int(''.join([digits[0], digits[-1]]))

@validate_arguments
def decode_input_line(inp: str) -> str:
    """Translate spelled out digits into actual digits"""
    if not inp:
        return ""
    for digit_str, digit in STR_TO_DIGIT.items():
        if inp.startswith(digit_str):
            return digit + decode_input_line(inp[1:])
    return inp[0] + decode_input_line(inp[1:])

if __name__ == '__main__':
    calibration_value = 0   # pylint: disable=C0103
    calibration_value_decoded = 0   # pylint: disable=C0103
    for inp_line in read_input():
        calibration_value += parse_calibration_values(inp_line)
        calibration_value_decoded += parse_calibration_values(decode_input_line(inp_line))
    print(f"Day 1, part 1: {calibration_value}")
    print(f"Day 1, part 2: {calibration_value_decoded}")
