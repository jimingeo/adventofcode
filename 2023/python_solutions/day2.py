#!/usr/bin/env python
"""
AoC 2023 Day 2
"""

import sys

from typing import List
from pydantic import BaseModel, validate_arguments

class GameInstance(BaseModel):
    """Represents one sample of cubes viewed"""
    red: int = 0
    green: int = 0
    blue: int = 0

class Game(BaseModel):
    """Represents one game"""
    game_id: int
    games: List[GameInstance]

def read_input():
    """Read input data from stdin"""
    for line in sys.stdin:
        yield line.rstrip('\n')

@validate_arguments
def parse_game_line(line: str) -> Game:
    """Parse a line of game data and build the Game object"""
    game_id_str, game_data = line.split(":")
    game_id = int(game_id_str.split(" ")[-1])
    games = [parse_game_inst(game_inst)
             for game_inst in game_data.split(";")]
    return Game(game_id=game_id, games=games)

@validate_arguments
def parse_game_inst(data: str) -> GameInstance:
    """Parse a sample of cubes viewed during a game and return a GameInstance object"""
    game_inst = {}
    for item in data.strip().split(','):
        val, color = item.split()
        # Assume color is going to be valid, won't be repeated in one input
        game_inst[color] = int(val)
    return GameInstance(**game_inst)

@validate_arguments
def is_game_valid(game: Game, game_constraint: GameInstance) -> bool:
    """Check if a game is valid based on a maximum possible cubes in bag"""
    for game_inst in game.games:
        if (game_inst.red > game_constraint.red or
            game_inst.green > game_constraint.green or
            game_inst.blue > game_constraint.blue):
            return False
    return True

@validate_arguments
def get_min_cubes_for_game(game: Game) -> int:
    """Returns the minimum number of cubes of every color required for a game to be valid"""
    game_min_requirement = GameInstance()
    for game_inst in game.games:
        game_min_requirement.red = max(game_min_requirement.red, game_inst.red)
        game_min_requirement.green = max(game_min_requirement.green, game_inst.green)
        game_min_requirement.blue = max(game_min_requirement.blue, game_inst.blue)
    return game_min_requirement.red * game_min_requirement.blue * game_min_requirement.green

if __name__ == "__main__":
    parsed_games = [parse_game_line(line) for line in read_input()]
    constraints = GameInstance(red=12, green=13, blue=14)
    valid_games = (game for game in parsed_games
                   if is_game_valid(game, constraints))
    valid_game_ids = (game.game_id for game in valid_games)
    print(f"Day 2, part 1: {sum(valid_game_ids)}")
    power_of_games = (get_min_cubes_for_game(game) for game in parsed_games)
    print(f"Day 2, part 2: {sum(power_of_games)}")
