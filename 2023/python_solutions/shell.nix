# Simple python environment
with import (fetchTarball(channel:nixos-22.05)) {};
let
  my-python-packages = python-packages: with python-packages; [
    mypy
    pylint
    pydantic
  ];

  python-with-my-packages = pkgs.python3.withPackages my-python-packages;

  packages = [
    python3
    python-with-my-packages
  ];

in mkShell {
  buildInputs = packages;
}
