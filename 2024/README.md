Solutions for AoC 2024 from https://adventofcode.com/2024

**Environment setup:**
The execution environment is configured with a simple Nix config to setup the necessary
environment, along with necessary supporting packages. Start the environment by running:
```shell
# nix-shell
```
in the language-specific directory

**Running the programs:**
The input files for each day is stored under `inputs/day<day>.txt`. The programs expect inputs from
stdin, so run the program by piping the file contents to stdin. For example:
```shell
# cat inputs/day1.txt | runhaskell Day1.hs
```
(or)
```shell
# runhaskell Day1.hs < inputs/day1.txt
```

Or for rust:
```shell
rustc day1.rs
cat ../inputs/day1.txt | ./day1
```
