package main

import (
    "bufio"
    "fmt"
    "os"
    "sort"
    "strings"
    "strconv"
)

func main() {
    lines := readInputLines()
    left, right := getLeftAndRight(lines)
    sort.Slice(left, func (i, j int) bool { return left[i] < left[j] })
    sort.Slice(right, func (i, j int) bool { return right[i] < right[j] })
    var sumOfDiff int = 0
    for i := range(left) {
        diff := left[i] - right[i]
        if diff < 0 {
            diff = -diff
        }
        sumOfDiff += diff
    }
    fmt.Println("Day 1 part 1:", sumOfDiff)

    var sumOfOccurrences int = 0
    for _, l := range(left) {
        sumOfOccurrences += l * countOccurences(right, l)
    }
    fmt.Println("Day 1 part 2:", sumOfOccurrences)
}

func readInputLines()([][]int) {
    lines := make([][]int, 0)
    scanner := bufio.NewScanner(os.Stdin)
    for {
        scanner.Scan()
        line := scanner.Text()
        if (len(line) != 0) {
            row := make([]int, 0)
            for _, r := range strings.Fields(line) {
                ri, _ := strconv.Atoi(r)
                row = append(row, ri)
            }
            lines = append(lines, row)
        } else {
            break
        }
    }
    return lines
}

func getLeftAndRight(lines [][]int)([]int, []int) {
    var left, right []int
    for _, line := range lines {
        left = append(left, line[0])
        right = append(right, line[1])
    }
    return left, right
}

func countOccurences(l []int, item int) int {
    var count int = 0
    for _, elem := range l {
        if elem == item {
            count += 1
        }
    }
    return count
}
