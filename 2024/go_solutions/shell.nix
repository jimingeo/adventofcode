with import <nixpkgs> {};
let
  packages = [
    go
  ];
in mkShell {
  buildInputs = packages;
}
