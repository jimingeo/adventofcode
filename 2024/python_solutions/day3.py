#!/usr/bin/env python
"""
AoC 2024 Day 3
"""

import re
import sys
from typing import Generator

def read_input() -> Generator[str, None, None]:
    """Read input data from stdin"""
    for line in sys.stdin:
        yield line.rstrip('\n')

def gather(inp_lines: list[str]) -> Generator[str, None, None]:
    """Gather mul instructions from the input data"""
    for inp_line in inp_lines:
        yield from re.findall(r"mul\(\d{1,3},\d{1,3}\)", inp_line)

def gather_with_conditionals(inp_lines: list[str]) -> Generator[str, None, None]:
    """Gather conditionals and mul instructions from the input data"""
    for inp_line in inp_lines:
        yield from re.findall(r"mul\(\d{1,3},\d{1,3}\)|do\(\)|don\'t\(\)", inp_line)

def run_mul_instruction(instruction: str) -> int:
    """Parse a `mul` instruction and return the result of the operation"""
    a, b = re.findall(r'mul\((\d+),(\d+)\)', instruction)[0]
    return int(a) * int(b)

def run_instructions(instructions: Generator[str, None, None]) -> Generator[int, None, None]:
    """Process instruction if not disabled"""
    mul_enabled = True
    for i in instructions:
        if i == "don't()":
            mul_enabled = False
        if i == "do()":
            mul_enabled = True
        if mul_enabled:
            if i.startswith('mul'):
                yield run_mul_instruction(i)

if __name__ == '__main__':
    input_instructions = list(read_input())
    sum_of_prod = sum((run_mul_instruction(instruction)
                       for instruction in gather(input_instructions)))
    print(f"Day 3, part 1: {sum_of_prod}")
    conditional_sum_of_product = sum(run_instructions(gather_with_conditionals(input_instructions)))
    print(f"Day 3, part 2: {conditional_sum_of_product}")
