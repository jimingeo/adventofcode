#!/usr/bin/env python
"""
AoC 2024 Day 7
"""

import sys
from typing import Generator

def read_input() -> Generator[str, None, None]:
    """Read input data from stdin"""
    for line in sys.stdin:
        yield line.rstrip('\n')

def gather(inp_lines: Generator[str, None, None]) -> Generator[tuple[int, list[int]], None, None]:
    """Gather result and equation from the input data"""
    for inp_line in inp_lines:
        res, operands = inp_line.split(':')
        yield int(res), [int(i) for i in operands.split()]

def is_equation_true(expected: int, operands: list[int], acc: int = 0) -> bool: #pylint: disable=R0911
    """Check if the equation can be true"""
    if len(operands) < 1:
        return False
    if len(operands) == 1:
        if acc + operands[0] == expected:
            return True
        if acc * operands[0] == expected:
            return True
        return False
    if is_equation_true(expected, operands[1:], acc + operands[0]):
        return True
    if is_equation_true(expected, operands[1:], (acc or 1) * operands[0]):
        return True
    return False

if __name__ == '__main__':
    input_data = gather(read_input())
    part1_res = sum((expected for expected, opers in input_data
                     if is_equation_true(expected, opers)))
    print(f"Day 7, part 1: {part1_res}")
    #print(f"Day 7, part 2: {sum_of_weighted_occurences}")
