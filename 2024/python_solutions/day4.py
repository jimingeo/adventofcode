#!/usr/bin/env python
"""
AoC 2024 Day 4
"""

import sys
from typing import Generator, Tuple

import numpy


def read_input() -> Generator[str, None, None]:
    """Read input data from stdin"""
    for line in sys.stdin:
        yield line.rstrip('\n')

def parse_input(inp_lines: Generator[str, None, None]) -> numpy.ndarray:
    """Parse the input data into a numpy array"""
    return numpy.array([list(line) for line in inp_lines])

def generate_valid_word_list(puzzle: numpy.ndarray) -> Generator[str, None, None]:
    """Generate a list of 4 letter words that are horizontal, vertical or diagonal"""
    def get_word_list(a4x4matrix: numpy.ndarray) -> Generator[numpy.ndarray, None, None]:
        """Generate horizontal, vertical and both diagonals of the 4x4 array"""
        yield a4x4matrix[0] # First row
        yield a4x4matrix[0:,0]  # First column
        yield a4x4matrix.diagonal() # Main diagonal
        yield numpy.fliplr(a4x4matrix).diagonal()   # The "anti-diagonal"
    for i in range(len(puzzle[0])): # Number of rows
        for j in range(len(puzzle[0:,0])): # Number of columns
            yield from (''.join(word) for word in
                        get_word_list(puzzle[i:i+4, j:j+4]) if len(word) == 4)

def is_match(word: str, comp: str) -> bool:
    """Check if a string is XMAS in either forward or reverse direction"""
    return True if word == comp else (word == ''.join(reversed(comp)))

def generate_3_diagonal_pairs(puzzle: numpy.ndarray) -> Generator[Tuple[str, str], None, None]:
    """Generate all 3 character diagonal pairs"""
    def get_diagonals(a3x3matrix: numpy.ndarray):
        """Get the diagonals for the 3x3 matrix"""
        yield (a3x3matrix.diagonal(), # Main diagonal
               numpy.fliplr(a3x3matrix).diagonal())   # The "anti-diagonal"
    for i in range(len(puzzle[0])): # Number of rows
        for j in range(len(puzzle[0:,0])): # Number of columns
            yield from ((''.join(d1), ''.join(d2)) for d1, d2 in
                        get_diagonals(puzzle[i:i+3, j:j+3])
                        if len(d1) == 3 and len(d2) == 3)

def is_diagonal_mas(diagonal: Tuple[str, str]) -> bool:
    """Check if the diagonals form an "X" mas"""
    d1, d2 = diagonal
    return is_match(d1, "MAS") and is_match(d2, "MAS")

if __name__ == '__main__':
    input_data = parse_input(read_input())
    xmas_count = len([word for word in
                      generate_valid_word_list(input_data)
                      if is_match(word, "XMAS")])
    print(f"Day 4, part 1: {xmas_count}")
    diagonal_mas_count = len([diag for diag in
                              generate_3_diagonal_pairs(input_data)
                              if is_diagonal_mas(diag)])
    print(f"Day 4, part 2: {diagonal_mas_count}")
