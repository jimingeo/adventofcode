#!/usr/bin/env python
"""
AoC 2024 Day 2
"""

import sys
from itertools import combinations, pairwise
from operator import sub
from typing import Generator

def read_input() -> Generator[str, None, None]:
    """Read input data from stdin"""
    for line in sys.stdin:
        yield line.rstrip('\n')

def gather(inp_lines: Generator[str, None, None]) -> Generator[list[int], None, None]:
    """Gather a slice of columns from the input data"""
    for inp_line in inp_lines:
        yield [int(item) for item in inp_line.split()]

def apply_problem_dampener(report: list[int]) -> list[int]:
    """Apply problem dampener to a report to see if removing one unsafe level will make it safe"""
    if is_report_valid(report):
        return report
    for dampened_report in combinations(report, len(report) - 1):
        if is_report_valid(list(dampened_report)):
            return list(dampened_report)
    return []

def is_report_valid(report: list[int]) -> bool:
    """Check if a report is valid"""
    level_diff = [sub(a, b) for (a, b) in pairwise(report)]
    if any(((abs(d) > 3 or abs(d) < 1) for d in level_diff)):
        return False
    if all((d > 0 for d in level_diff)):
        return True
    if all((d < 0 for d in level_diff)):
        return True
    return False

if __name__ == '__main__':
    reports = list(gather(read_input()))
    valid_reports = [report for report in reports if is_report_valid(report)]
    print(f"Day 1, part 1: {len(valid_reports)}")
    valid_dampened_reports = [report for report in reports if apply_problem_dampener(report)]
    print(f"Day 1, part 2: {len(valid_dampened_reports)}")
