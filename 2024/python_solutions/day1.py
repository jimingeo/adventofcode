#!/usr/bin/env python
"""
AoC 2024 Day 1
"""

import sys
from operator import countOf
from typing import Generator

def read_input() -> Generator[str, None, None]:
    """Read input data from stdin"""
    for line in sys.stdin:
        yield line.rstrip('\n')

def gather(inp_lines: list[str], pos: int) -> Generator[int, None, None]:
    """Gather a slice of columns from the input data"""
    for inp_line in inp_lines:
        items = inp_line.split()
        yield int(items[pos])

if __name__ == '__main__':
    input_data = list(read_input())
    left = list(gather(input_data, 0))
    right = list(gather(input_data, 1))
    sum_of_diff = sum(abs(l-r) for l, r in zip(sorted(left), sorted(right)))
    print(f"Day 1, part 1: {sum_of_diff}")
    sum_of_weighted_occurences = sum(l * countOf(right, l) for l in left)
    print(f"Day 1, part 2: {sum_of_weighted_occurences}")
