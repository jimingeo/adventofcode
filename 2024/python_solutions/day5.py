#!/usr/bin/env python
"""
AoC 2024 Day 5
"""

import sys
from collections import defaultdict
from itertools import takewhile
from typing import Generator, Iterable

def read_input() -> Generator[str, None, None]:
    """Read input data from stdin"""
    for line in sys.stdin:
        yield line.rstrip('\n')

def build_page_ordering_rule(rules: Iterable[str]) -> dict[str, list[str]]:
    """Transform the ordering rule into a list of page numbers in allowed order"""
    ordering_rule_list = defaultdict(list)
    for rule in rules:
        before_page, after_page = rule.split("|")
        ordering_rule_list[after_page].append(before_page)
    return ordering_rule_list

def do_pages_follow_rule(pages: list[str], rules: dict[str, list[str]]) -> bool:
    """Check if a list of pages to print follow the given set of rules"""
    if not pages:   # No more pages left to check
        return True
    if pages[0] in rules.keys():
        if any((page in rules[pages[0]] for page in pages[1:])):
            return False
    return do_pages_follow_rule(pages[1:], rules)

def fix_page_order(pages: list[str], rules: dict[str, list[str]]) -> list[str]:
    """Fix the page order by applying the rules"""
    if len(pages) < 2:
        return pages
    if do_pages_follow_rule(pages, rules):
        return pages
    if pages[0] in rules.keys():
        preceeding_pages: list[str] = []
        unaffected_pages: list[str] = []
        for p in pages[1:]:
            if p in rules[pages[0]]:
                preceeding_pages.append(p)
            else:
                unaffected_pages.append(p)
        if not preceeding_pages:
            return [pages[0]] + fix_page_order(pages[1:], rules)
        return fix_page_order(preceeding_pages + [pages[0]] + unaffected_pages, rules)
    return [pages[0]] + fix_page_order(pages[1:], rules)

if __name__ == '__main__':
    input_data = read_input()
    ordering_rules = build_page_ordering_rule(
        takewhile(lambda line: line, input_data)) # Take till empty line
    pages_to_print = [pages.split(',') for pages in input_data]   # remaining data in the generator
    valid_pages = (pages for pages in pages_to_print if do_pages_follow_rule(pages, ordering_rules))
    middle_page = (int(pages[len(pages) // 2]) for pages in valid_pages)
    print(f"Day 5, part 1: {sum(middle_page)}")
    invalid_pages = (pages for pages in pages_to_print
                     if not do_pages_follow_rule(pages, ordering_rules))
    reordered_pages = (fix_page_order(page, ordering_rules) for page in invalid_pages)
    middle_page = (int(pages[len(pages) // 2]) for pages in reordered_pages)
    print(f"Day 5, part 2: {sum(middle_page)}")
