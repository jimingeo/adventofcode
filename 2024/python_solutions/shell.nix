# Simple python environment
with import (fetchTarball(channel:nixos-24.11)) {};
let
  my-python-packages = python-packages: with python-packages; [
    mypy
    numpy
    pylint
  ];

  python-with-my-packages = pkgs.python3.withPackages my-python-packages;

  packages = [
    entr
    python3
    python-with-my-packages
  ];

in mkShell {
  buildInputs = packages;
}
